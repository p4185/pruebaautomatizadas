package Base;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import Code.ingresoEmail;

public class Youtube2 {
	
	WebDriver driver;
	ExtentReports extent;
	@BeforeSuite
	public void beforeSuite() {
		    
	        // create ExtentReports and attach reporter(s)
	        ExtentReports extent = new ExtentReports();
		
	}
	

	
	@BeforeClass
	public void setup() {
		System.setProperty("webdriver.gecko.driver","./src/test/resources/firefox/geckodriver.exe");

		
	}

	@Test
	public void testeo() throws InterruptedException {
		

		ExtentTest test = extent.createTest("Youtube ", "Sample description");
        test.log(Status.INFO, "This step shows usage of log(status, details)"); 
        test.info("This step shows usage of info(details)");
        
        WebDriver driver = new FirefoxDriver();

		driver.manage().window().maximize();

		driver.get("https://www.youtube.com/watch?v=gWSqQPla4sE&t=6s&ab_channel=MRC24");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;


		ingresoEmail elegir = new ingresoEmail(driver);

		
		elegir.ingreso();
		elegir.play();
		
		elegir.scrolleoParaAbajo();
		
		elegir.comentar();
		elegir.scrolleoParaAbajo();
		elegir.salirYT();
		elegir.salir();
		
		
	}
	@AfterClass
	public void salida() {
		System.out.print(" -TERMINO EL TESTEOOO");
		driver.quit();
	}
	  public void after() {
	    	extent.flush();
	    }
	

}
