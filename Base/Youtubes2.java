package Base;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import Code.ingresoEmail;

public class Youtubes2 {
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest extentTest;
	WebDriver driver ;
	By imagen2 = By.id("logo-icons");
	@BeforeClass
	public void setup() {
		System.setProperty("webdriver.gecko.driver","./src/test/resources/firefox/geckodriver.exe");
		
		
		htmlReporter = new ExtentHtmlReporter("./reports/yt.html");
		htmlReporter.config().setEncoding("utf-8");
		htmlReporter.config().setDocumentTitle("Automatizacion reportes");
		
		htmlReporter.config().setReportName("Resultados de los test realizados");
		htmlReporter.config().setTheme(Theme.STANDARD);
		
		extent = new ExtentReports();
		extent.setSystemInfo("organizacion", "Actualizacion");
		extent.setSystemInfo("Firefox", "Buscador");
		extent.attachReporter(htmlReporter);
		System.setProperty("webdriver.gecko.driver","./src/test/resources/firefox/geckodriver.exe");

		driver = new FirefoxDriver();
		driver.get("https://www.youtube.com/watch?v=gWSqQPla4sE&t=8s&ab_channel=MRC24");

		driver.manage().window().maximize();

		

	    

	}
	@AfterMethod
	public void after(ITestResult result) {
		String methodName = result.getMethod().getMethodName();
		if(result.getStatus() == ITestResult.FAILURE) {
			String exception = Arrays.toString(result.getThrowable().getStackTrace());
			extentTest.fail("<details><summary><b><font color = red><Exception Occured , click to see details:>"+"</font></b></summary>"+exception.replaceAll(",", "<br>")+"</details\n");
			String path = takeScreenshot(result.getMethod().getMethodName());
			try {
				extentTest.fail("<b><font color = red>"+"Screenshot of failure"+"</font></b>",MediaEntityBuilder.createScreenCaptureFromPath(path).build());
				
			}catch(IOException e) {
				extentTest.fail("Test fallado");
				
				
			}
			String logText = "<b>Test metodo" + methodName + "failed</b>";
			Markup m = MarkupHelper.createLabel(logText, ExtentColor.RED);
			extentTest.log(Status.FAIL, m);
			
		}else if(result.getStatus() == ITestResult.SUCCESS) {
			String logText = "<b>Test metodo" + methodName + "Passed</b>";
			Markup m = MarkupHelper.createLabel(logText, ExtentColor.GREEN);
			extentTest.log(Status.PASS	, m);
		}else if(result.getStatus() == ITestResult.SKIP) {
			String logText = "<b>Test metodo" + methodName + "Skipeado</b>";
			Markup m = MarkupHelper.createLabel(logText, ExtentColor.YELLOW);
			extentTest.log(Status.SKIP	, m);
		
		}
	}
	public String takeScreenshot(String methodName) {
		String fileName = getScreenshotName(methodName);
		String directory = System.getProperty("user.dir")+"/screenshots/";
		new File(directory).mkdirs();
		String path = directory + fileName;
		try {
			File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(screenshot, new File(path));
			System.out.println("************************");
			System.out.println("Capturada la imagen : " + path);
			System.out.println("************************");

			
		}catch(Exception e) {
			e.printStackTrace();
		
		}
		return path;
	}
	public static String getScreenshotName(String methodName) {
		Date d = new Date();
		String fileName = methodName + "_" + d.toString().replace(":", "_").replace(" ", "_")+ ".png";
		return fileName;
		
	}
	
	
	

	@Test (priority = 1)

	public void test1() throws Exception {

		try {



			ingresoEmail elegir = new ingresoEmail(driver);

			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;

		
			elegir.ingreso();
			elegir.play();
		
			elegir.scrolleoParaAbajo();
		
			elegir.comentar();
			elegir.scrolleoParaAbajo();
			elegir.salirYT();
			elegir.salir();
			extentTest = extent.createTest("\"Youtube\", \"Ingresa a youtube , busca un video y entra a un link\"");
			extentTest.log(Status.PASS, "El test paso correctamente");



		}catch(Exception ex) {
			extentTest = extent.createTest("Test fallado");
			extentTest.log(Status.FAIL, "El test no  paso correctamente");
			Assert.fail("ejecutando el test fallado");
			driver.quit();
		}
	}
	@Test (priority = 2)

	public void primeraFalla() throws InterruptedException {
			driver = new FirefoxDriver();

			driver.get("https://www.youtube.com/watch?v=gWSqQPla4sE&t=6s&ab_channel=MRC24");
			driver.manage().window().maximize();
			

			try {
				ingresoEmail elegir = new ingresoEmail(driver);
				driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;

				
				elegir.ingreso();
				elegir.play();
				
				elegir.scrolleoParaAbajo();
				
				elegir.comentar();
				elegir.scrolleoParaAbajo();
			
				if(driver.findElement(imagen2).isDisplayed());
					elegir.salirYT();
					elegir.salir();
					extentTest = extent.createTest("\"Youtube\", \"Ingresa a youtube , busca un video y entra a un link\"");
					extentTest.log(Status.PASS, "El test paso correctamente");

				
			}catch(Exception ex){

				extentTest = extent.createTest("\"Youtube\", \"Ingresa a youtube , busca un video y entra a un link\"");
				extentTest.log(Status.FAIL, "El test no  paso correctamente");
				Assert.fail("ejecutando el test fallado");
				
			}
			


				
		
	        
				
			}
		@AfterClass
		public void baja() {
			extent.flush();
			driver.quit();
		}
		
		
  }

