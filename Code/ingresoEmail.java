package Code;





import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;

public class ingresoEmail {
	WebDriver driver;
	By entrar = By.xpath("//ytd-button-renderer[@class='style-scope ytd-masthead style-suggestive size-small']");
	By email = By.id("identifierId");
	By botonNext = By.xpath("//div[@class='VfPpkd-dgl2Hf-ppHlrf-sM5MNb']//button[@type='button']");
	By password = By.cssSelector("input[class = 'whsOnd zHQkBf']");
	By texto = By.id("headingText");
	By next2 = By.xpath("//div[@id='passwordNext']//div[@class='VfPpkd-dgl2Hf-ppHlrf-sM5MNb']//button[@type='button']");
	By playButon = By.xpath("//button[@class='ytp-play-button ytp-button']");
	By comentario = By.cssSelector("#placeholder-area ");
	By comento = By.xpath("//div[@id='contenteditable-root']");
	By publico = By.xpath("//ytd-comments-header-renderer/div[@id='simple-box']/ytd-comment-simplebox-renderer[1]/div[3]/ytd-comment-dialog-renderer[1]/ytd-commentbox[1]/div[1]/div[4]/div[5]/ytd-button-renderer[2]/a[1]");
	By volverYt = By.xpath("//ytd-masthead/div[@id='container']/div[@id='start']/ytd-topbar-logo-renderer[@id='logo']/a[@id='logo']/div[1]/yt-icon[1]");
	By botonExplor = By.xpath("//body/ytd-app[1]/div[1]/ytd-mini-guide-renderer[1]/div[1]/ytd-mini-guide-entry-renderer[2]/a[1]");
	By salir = By.cssSelector("ytd-popup-container.style-scope.ytd-app:nth-child(13) tp-yt-iron-dropdown.style-scope.ytd-popup-container:nth-child(2) div.style-scope.tp-yt-iron-dropdown ytd-multi-page-menu-renderer.style-scope.ytd-popup-container div.menu-container.style-scope.ytd-multi-page-menu-renderer:nth-child(3) div.style-scope.ytd-multi-page-menu-renderer:nth-child(1) yt-multi-page-menu-section-renderer.style-scope.ytd-multi-page-menu-renderer:nth-child(1) div.style-scope.yt-multi-page-menu-section-renderer:nth-child(2) ytd-compact-link-renderer.style-scope.yt-multi-page-menu-section-renderer:nth-child(5) a.yt-simple-endpoint.style-scope.ytd-compact-link-renderer > tp-yt-paper-item.style-scope.ytd-compact-link-renderer");
	By imagen2 = By.xpath("//ytd-masthead/div[@id='container']/div[@id='end']/div[@id='buttons']/ytd-topbar-menu-button-renderer[3]/button[1]/yt-img-shadow[1]/img[1]");

	public ingresoEmail(WebDriver driver) {
		this.driver = driver;
		
	}
	
	public void ingreso() throws InterruptedException {
		driver.findElement(entrar).click();
		driver.findElement(email).sendKeys("aca se ingresa el mail");
		driver.findElement(botonNext).click();
		Thread.sleep(2000);
		
		driver.findElement(password).sendKeys("Contraseņa");
		driver.findElement(next2).click();
		
		
			
		
	}
	public void play() throws InterruptedException {
		driver.findElement(playButon).click();
		
		driver.findElement(playButon).click();
	}
	public void scrolleoParaAbajo() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,500);");
	}
	public void comentar() {
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;

		driver.findElement(comentario).click();
		driver.findElement(comento).sendKeys("Golazzoooo");
		driver.findElement(publico).click();
		
	}
	
	public void scrolleoParaArriba() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,-500);");
		
		
	}
	public void salirYT() throws InterruptedException {
		
		driver.findElement(imagen2).click();
		//@SuppressWarnings("deprecation")
		//Wait<WebDriver> fwait1 = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS)
		//.pollingEvery(2, TimeUnit.SECONDS)
		//.ignoring(NoSuchElementException.class);

		//WebElement video1 = fwait1.until(new Function<WebDriver, WebElement>() {
		//public WebElement apply(WebDriver driver1) {
		//return driver1.findElement(salir);
		//}
		//});

		assertTrue(driver.findElement(salir).isDisplayed());
		  
		driver.findElement(salir).click();
		
		
	}
	
	public void salir() throws InterruptedException {
		
		driver.quit();
		
	}
}
