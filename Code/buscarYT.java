package Code;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
public class buscarYT {
	WebDriver driver;
	By buscar = By.name("search_query");
	By aceptoBusqueda = By.id("search");
	By imagen = By.xpath("//img[@src='https://i.ytimg.com/vi/gWSqQPla4sE/hq720.jpg?sqp=-oaymwEcCOgCEMoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLD_I2mJ0H-8wT9Is9lt3CmTm_IwUA']");
	By ingresarlink2 = By.linkText("Top 10 GOLAZOS de LIONEL MESSI con la selecci�n Argentina en la d�cada (2010 - 2019)");
	By icono = By.id("logo-icon");
	public buscarYT (WebDriver driver) {
		 this.driver=driver;
	 }
	 public void buscador() {
		 driver.findElement(buscar).sendKeys("gol de messi");
		 driver.findElement(buscar).submit();
	 }
	 
	 public void entrar() throws InterruptedException {
		 
		 assertTrue(driver.findElement(imagen).isDisplayed());
		 driver.findElement(ingresarlink2).click();
		 
	 }
	 public void Salir() throws InterruptedException {
		 JavascriptExecutor jse = (JavascriptExecutor) driver;
		 jse.executeScript("window.scrollBy(0,200);");
		 if(driver.findElement(icono).isDisplayed())
			 Thread.sleep(2000);
		 	 driver.quit();
	 }

}
